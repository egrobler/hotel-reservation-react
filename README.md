# Hotel Reservation App
`npm install`

## To Run the application
`npm run dev`  -> `http://localhost:8000`


****************///Routes Start///*******************

### GET /reservation/ID – Returns a single reservation with ID
    - Navigate to http://localhost:8000/reservation/5a8fbbb0799d1f6f40c20995

### POST /reservation – Creates a new reservation, assigns an ID to it, and returns that ID
    - Use the http CLI tool or Postman or similar

### GET /reservations – Returns all reservations
    - Navigate to http://localhost:8000/reservations

`http POST http://localhost:8000/reservation name=Sasi hotelName="Hilton Garden Inn Mountain View" arrivalDate=04/01/2018 departureDate=04/08/2018`

###Reservations should be persisted into a NoSQL store (Persisted to mongodb using mLab)
`mongodb://ettiene:pass123!!@ds159184.mlab.com:59184/hotel-reservation`

## Graphiql interface
- `http://localhost:8000/graphiql`

## BONUS: GET '/reservations?hotelName=X&arrivalDate=Y&departureDate=Z' - Returns all reservations that match the search criteria
- `http://localhost:8000/reservations?hotelName=Sasi&arrivalDate=2018-03-01&departureDate=2018-03-10`


## Productionize - I have used 'NOW' to access the app live. Since it is free account it does not allow package size larget than 1MB
- `npm run dev` to run it on Local

## React UI
- Created Simple UI for Get/Add reservations 

### Screenshots:
Added screenshots in "Screenshots" folder to see what it looks like

****************///Assessment End///*******************

## Testing methods
I used the CLI tool from <httpie.org> to test the functionality via the graphiql interface. Install with `brew install httpie` and then run the following to test:

### Queries
#### All Reservations example
    {
      reservations {
        id
        name
        hotelName
        arrivalDate
        departureDate
      }
    }

#### Reservations By ID example
    {
      reservation(id: "5a8ef2c112002468cafe9f17") {
        id
        name
        hotelName
        arrivalDate
        departureDate
      }
    }


### Add Booking/reservation mutation (only returns the ID)
    mutation {
      makeReservation(
        name: "Test Test",
        hotelName: "Hilton Hotel Nr1",
        arrivalDate: "03/01/2018",
        departureDate: "03/10/2018"
      ) {
        id
      }
    }


### Knowledge Base:

Foundation files are referred from learnnode.com
Created MongoDb Cluster at mlab.com

### Tech Stack:

Reactjs
express
Webpack
GraphQL
Nodejs
MongoDB
AWS
Bootstrap

